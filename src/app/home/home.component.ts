import { Component } from '@angular/core';
import { PostComponent } from '../post/post.component';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
  imports: [PostComponent, RouterLink],
})
export class HomeComponent {}
