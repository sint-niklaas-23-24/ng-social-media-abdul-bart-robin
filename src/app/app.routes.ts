import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { UserProfileDetailComponent } from './user-profile-detail/user-profile-detail.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'post-detail/:post-id', component: PostDetailComponent },
  { path: 'user-detail/:user-id', component: UserProfileDetailComponent },
];
