import { Component } from '@angular/core';
import { PostService } from '../services/post.service';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-post-detail',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './post-detail.component.html',
  styleUrl: './post-detail.component.css'
})
export class PostDetailComponent {
  detailedPost: any;
  user: any;

  constructor(private postService: PostService, private userService: UserService, private route: ActivatedRoute) {
    this.route.params.subscribe(parameters => {
      const postID = parameters['post-id'];
      this.postService.getPostByID(postID).subscribe(response => this.detailedPost = response);
      this.userService.getUserID(this.detailedPost.userId).subscribe(response => this.user = response);
      console.log(this.user);
    });
  }
}
