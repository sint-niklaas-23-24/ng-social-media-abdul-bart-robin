import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  constructor(private httpClientPosts: HttpClient) { }

  getPosts() {
    return this.httpClientPosts.get('https://jsonplaceholder.org/posts');
  }

  getPostByID(ID: number) {
    return this.httpClientPosts.get(`https://jsonplaceholder.org/posts/${ID}`);
  }
}
