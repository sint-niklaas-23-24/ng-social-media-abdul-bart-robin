import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClientUsers: HttpClient) { }

  getUsers() {
    return this.httpClientUsers.get('https://jsonplaceholder.org/users');
  }

  getUserID(ID: number) {
    return this.httpClientUsers.get(`https://jsonplaceholder.org/users/${ID}`);
  }
}
