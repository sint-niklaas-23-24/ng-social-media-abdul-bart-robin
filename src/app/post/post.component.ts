import { Component } from '@angular/core';
import { PostService } from '../services/post.service';
import { RouterLink } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-post',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './post.component.html',
  styleUrl: './post.component.css',
})
export class PostComponent {
  posts: any = [];

  constructor(private postService: PostService) {
    this.postService.getPosts().subscribe((response) => {
      this.posts = response;
      console.log(this.posts);
    });
  }
}
